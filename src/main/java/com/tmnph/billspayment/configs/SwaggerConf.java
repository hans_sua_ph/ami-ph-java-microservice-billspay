package com.tmnph.billspayment.configs;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.truemoneyph.config.SwaggerConfig;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
//@EnableSwagger2
public class SwaggerConf extends SwaggerConfig {
	@Bean
	@Override
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.tmnph.billspayment.test.controller")).paths(regex("/test.*"))
				.build().apiInfo(super.apiInfo());
	}
//
//	private ApiInfo metaData() {
//		ApiInfo apiInfo = new ApiInfo("Test POC", "POC for microservices", "1.0", "Terms of service",
//				new Contact("Hans Suarez", "", "hans.sua@ascendcorp.com"), "Apache License Version 2.0",
//				"https://www.apache.org/licenses/LICENSE-2.0");
//		return apiInfo;
//	}
}