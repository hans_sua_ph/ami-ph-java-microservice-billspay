package com.tmnph.billspayment.configs;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import com.truemoneyph.common.util.ErrorCodeUtil;

/**
 * For getting the values in the property folder This will be used for the
 * Response Error Code {@link PortalErrorCodeTest} - sample test for portal
 * error config
 */
@Component
@RefreshScope
public class PortalErrorCodeConfig extends ErrorCodeUtil{

}
