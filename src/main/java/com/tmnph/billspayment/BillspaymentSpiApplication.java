package com.tmnph.billspayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class BillspaymentSpiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillspaymentSpiApplication.class, args);
	}
}
