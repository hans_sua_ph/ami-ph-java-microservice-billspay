package com.tmnph.billspayment.webservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.truemoneyph.common.util.DateUtility;
import com.truemoneyph.common.util.EncryptorDecryptorUtil;
import com.truemoneyph.common.util.JsonUtil;

import ECPay.wsdl.ArrayOfBStruct;
import ECPay.wsdl.ECPNBillsPaymentService;
import ECPay.wsdl.ECPNBillsPaymentServiceSoap;
import ECPay.wsdl.TransactStatus;
import ECPayTopUp.wsdl.ArrayOfTStruct;
import ECPayTopUp.wsdl.LoginDetails;
import ECPayTopUp.wsdl.TransactionStatus;
import ECPayTopUp.wsdl.WSTopUp;
import ECPayTopUp.wsdl.WSTopUpSoap;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ECPayWebService {
	@Value("${ecp_accountID}")
	private String accountID;
	@Value("${ecp_userName}")
	private String username;
	@Value("${ecp_password}")
	private String password;
	@Value("${ecp_branchID}")
	private String branchId;

	private ECPNBillsPaymentService paymentService;
	private ECPNBillsPaymentServiceSoap ecPayBills;
	private LoginDetails loginDetails;
	private static WSTopUp topUpService;
	private static WSTopUpSoap ecPayTopUp;

	@Autowired
	public ECPayWebService() {
		this.paymentService = new ECPNBillsPaymentService();
		this.topUpService = new WSTopUp();

		this.loginDetails = new LoginDetails();
		this.loginDetails.setAccountID(accountID);
		this.loginDetails.setBranchID(branchId);
		this.loginDetails.setUsername(username);
		this.loginDetails.setPassword(password);
	}

	public ArrayOfBStruct getBillerList() {
		ecPayBills = paymentService.getECPNBillsPaymentServiceSoap();
		log.info("test: " + this.getClass());
		ArrayOfBStruct test = ecPayBills.getBillerList(accountID, username, password);
		log.info("test: " + test);
//		ben: commented out the method JsonUtil.objectToJson() since the method is having an error
//		log.info("test: " + JsonUtil.objectToJson(test));
		return test;

	}

	/**
	 * Get balance
	 * 
	 * @return AccountBal
	 */
	private String ECPay_checkBalance() {
		ecPayBills = paymentService.getECPNBillsPaymentServiceSoap();
		try {
			return ecPayBills.checkBalance(accountID, username, password).getRemBal();
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return "";
	}

	private String ECPay_changepassword(String oldpassword, String newpassword, String reTypeNewpassword) {
		ecPayBills = paymentService.getECPNBillsPaymentServiceSoap();
		;
		try {
			return ecPayBills.changePassword(accountID, username, newpassword, reTypeNewpassword, oldpassword)
					.getAccountInfo();
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return null;
	}

	private ArrayOfBStruct ECPay_getBillerList() {
		ecPayBills = paymentService.getECPNBillsPaymentServiceSoap();

		try {
			ArrayOfBStruct biller = ecPayBills.getBillerList(accountID, username, password);
			return biller;
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return null;
	}

	public String ECPay_validateAccount(String identifier, String billerTag, String amount) {
		ecPayBills = paymentService.getECPNBillsPaymentServiceSoap();
		;
		try {
			return ecPayBills.validateAccount(accountID, username, password, accountID, identifier, billerTag, amount);
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return "";
	}

	public TransactStatus ECPay_transact(String accountNo, String identifier, String billerTag, String amount) {
		ecPayBills = paymentService.getECPNBillsPaymentServiceSoap();
		;
		String clientReference = DateUtility.getCurrentTimestamp().toString();
		try {
			return ecPayBills.transact(accountID, branchId, username, password, accountNo, identifier, billerTag,
					amount, clientReference);
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}

		return null;
	}
//	public static DuplicateMessagePerCode ConvertECPayMessage(string status, string message)
//    {
//        try
//        {
//            DuplicateMessagePerCode dup = new DuplicateMessagePerCode();
//
//            if (status == "1014" || status == "2014")
//            {
//                if (message.ToLower().Contains("TRANSACTION CANNOT BE PROCESSED. AMOUNT TO BE PAID SHOULD BE A TOTAL AMOUNT OF".ToLower()))
//                {
//                    dup.Status = status;
//                    string endline = message.ToLower().Contains(",".ToLower()) ? "," : "..";
//                    string amount = UtilityHelper.Between(message + "..", "OF ", endline);
//
//                    string info1 = amount.ToString();
//
//                    if (message.ToLower().Contains("OR BALANCE OF".ToLower()))
//                    {
//                        string amount2 = UtilityHelper.Between(message + "..", "OR ", "..");
//                        amount2 = UtilityHelper.Between(amount2 + "..", "OF ", "..");
//                        info1 = info1 + string.Format(", |or balance of |{0}", amount2);
//                    }
//
//                    dup.Message = string.Format(ThirdPartyErrorCode.getErrorMessage(1023), info1);
//                    dup.IsDuplicate = true;
//                }
//                else if (message.Contains("NO PENDING PAYMENT"))
//                {
//                    dup.Status = status;
//                    dup.Message = ThirdPartyErrorCode.getErrorMessage(1024);
//                    dup.IsDuplicate = true;
//                }
//
//            }
//            return dup;
//        }
//        catch (Exception e)
//        {
//            Log.Debug("ECPayServices_ConvertECPayMessage : " + e.Message + " " + e.InnerException);
//            return null;
//        }
//    }

	/*ECPAY TOPUP*/

	public String ECPayTopUp_checkBalance() {
		ecPayTopUp = topUpService.getWSTopUpSoap();
		try {
			return ecPayTopUp.checkBalance(loginDetails).getRemBal();
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return "";
	}

	public String ECPayTopUp_changepassword(String newpassword, String reTypeNewpassword) {
		ecPayTopUp = topUpService.getWSTopUpSoap();
		try {
			return ecPayTopUp.changePassword(loginDetails, newpassword, reTypeNewpassword).getAccountInfo();
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return "";

	}

	public ArrayOfTStruct ECPayTopUp_getTelcoList() {
		ecPayTopUp = topUpService.getWSTopUpSoap();
		ArrayOfTStruct telco = new ArrayOfTStruct();
		try {
			telco = ecPayTopUp.getTelcoList(loginDetails);
			return telco;
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return null;

	}

	public TransactionStatus ECPayTopUp_transact(String telco, String mobileNo, String extTag, String amount) {
		ecPayTopUp = topUpService.getWSTopUpSoap();
		String token, md5;
		TransactionStatus transactTopUp = new TransactionStatus();
		try {
			md5 = loginDetails.getBranchID() + mobileNo + amount + DateUtility.getCurrentDate("MMddyy");
			token = EncryptorDecryptorUtil.createMD5(md5);

			transactTopUp = ecPayTopUp.transact(loginDetails, telco, mobileNo, extTag, amount, token.toLowerCase());
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.getMessage());
		}
		return transactTopUp;
	}
	
//	public static string convertStatusCode(string statusCode, string statusMessage = "")
//    {
//        statusMessage = (statusMessage == null) ? string.Empty : statusMessage.Trim();
//
//        //the comment part is the equivalent third party error message
//        if (statusCode == "1") statusCode = "4001"; //Topup Failed
//        else if (statusCode == "2") statusCode = "4002"; //Topup Failed
//        else if (statusCode == "3") statusCode = "4003"; //Smart Unavailable
//        else if (statusCode == "4") statusCode = "4004"; //Telco Unavailable
//        else if (statusCode == "5") statusCode = "4005"; //Topup Failed
//        else if (statusCode == "6") statusCode = "4006"; //Topup Request Failed
//        else if (statusCode == "7") statusCode = "4007"; //Internal Error
//        else if (statusCode == "8") statusCode = "4008"; //Topup Failed
//        else if (statusCode == "9") statusCode = "4009"; //Invalid Client XML Format
//        else if (statusCode == "10") statusCode = "4010"; //Invalid Class ID
//        else if (statusCode == "11") statusCode = "4011"; //Invalid Account
//        else if (statusCode == "12") statusCode = "4012"; //Invalid Authentication
//        else if (statusCode == "13") statusCode = "4013"; //Invalid New Password
//        else if (statusCode == "14") statusCode = "4014"; //Invalid Session ID
//        else if (statusCode == "15") statusCode = "4015"; //Invalid Amount
//        else if (statusCode == "16") statusCode = "4016"; //Invalid Target Account
//        else if (statusCode == "17") statusCode = "4017"; //Invalid Payee
//        else if (statusCode == "18") statusCode = "4018"; //Invalid Result Code
//        else if (statusCode == "19") statusCode = "4019"; //Insufficient Balance
//        else if (statusCode == "20") statusCode = "4020"; //Error Date Format
//        else if (statusCode == "21") statusCode = "4021"; //Invalid Query Mode
//        else if (statusCode == "22") statusCode = "4022"; //Invalid TransType
//        else if (statusCode == "23") statusCode = "4023"; //Smart Disabled
//        else if (statusCode == "24") statusCode = "4024"; //Globe Disabled
//        else if (statusCode == "25") statusCode = "4025"; //Discount Price Error
//        else if (statusCode == "26") statusCode = "4026"; //Access Denied
//        else if (statusCode == "27") statusCode = "4027"; //Account Type Error
//        else if (statusCode == "28") statusCode = "4028"; //Waiting for Telco Reply
//        else if (statusCode == "29") statusCode = "4029"; //Cancelled Transaction
//        else if (statusCode == "30") statusCode = "4030"; //Second Attempt Too Soon
//        else if (statusCode == "38") statusCode = "4038"; //Non-Existent/Suspended/Inactive/Barred No
//
//        return statusCode;
//    }

}
