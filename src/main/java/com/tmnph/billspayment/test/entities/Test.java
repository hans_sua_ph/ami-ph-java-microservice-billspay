package com.tmnph.billspayment.test.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.truemoneyph.mvc.base.entity.BaseEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Entity
@Table(name = "test")
@AttributeOverride(name = "id", column = @Column(name = "id"))
@Data
public class Test extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "The parameters", required = true)
	@Column(name = "param",columnDefinition = "TEXT", length = 500)
	private String param;
}
