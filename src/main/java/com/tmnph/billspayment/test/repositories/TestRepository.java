package com.tmnph.billspayment.test.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tmnph.billspayment.test.entities.Test;
import com.truemoneyph.mvc.base.repository.BaseJPARepository;
@Repository
public interface TestRepository extends BaseJPARepository<Test, Long> {
	@Query("select a from Test a where a.id = ?1")
	public Test findFirstById(Long id);
}
