package com.tmnph.billspayment.test.dto;

import com.tmnph.billspayment.test.entities.Test;
import com.tmnph.billspayment.test.generic.ResponseObject;

import java.util.List;
import java.util.Map;

public class TestResponseMap extends ResponseObject<Map<String, String>> {
    @Override
    public Map<String, String> getData() {
        return (Map<String, String>) this.data;
    }

    @Override
    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
