package com.tmnph.billspayment.test.dto;

import ECPay.wsdl.BStruct;
import com.tmnph.billspayment.test.generic.ResponseObject;

import java.util.List;

public class TestResponse extends ResponseObject<List<BStruct>> {
    @Override
    public List<BStruct> getData() {
        return (List<BStruct>) this.data;
    }

    @Override
    public void setData(List<BStruct> data) {
        this.data = data;
    }
}
