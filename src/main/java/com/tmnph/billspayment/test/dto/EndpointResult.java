package com.tmnph.billspayment.test.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

public class EndpointResult<T> {
	@JsonIgnore
	public final String OK = "ok";
	@JsonIgnore
	public final String FAIL = "fail";

	@Getter
	private String status = OK;

	@Getter
	private String error;

	@Getter
	@Setter
	private T data;

	public EndpointResult() {
		setOk();

	}

	public EndpointResult(EndpointResult<T> result) {
		addResult(result);
	}

	/**
	 * Merge the fields of an Endpoint result to this. New input takes precedence
	 * over existing.
	 * 
	 * @param result
	 */
	public void addResult(EndpointResult<T> result) {
		this.status = result.status;
		this.error = result.error;
		this.data = result.data;
	}

	/**
	 * Add just data to this result.
	 * 
	 * @return
	 */
	public void addData(T data) {
		this.data = data;
	}

	public void setOk() {
		status = OK;
	}

	public void setError() {
		status = FAIL;
	}

	public void addError(String error) {
		setError();
		this.error = error;
	}

	public Object getData(String key) {
		// TODO: get a member of an object
		return null;
	}

	public boolean hasErrors() {
		return (status == FAIL);
	}

	/**
	 * Set your own unique data key for the result set. Used if you don't want to
	 * use the default "data" key being returned.
	 * 
	 * @param key
	 * @param value
	 */
	public void addData(String key, Object value) {
		if (value != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(key, value);
			this.data = (T) map;
		}
	}

}

