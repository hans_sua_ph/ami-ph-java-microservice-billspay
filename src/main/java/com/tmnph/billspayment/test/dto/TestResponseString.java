package com.tmnph.billspayment.test.dto;

import ECPay.wsdl.BStruct;
import com.tmnph.billspayment.test.entities.Test;
import com.tmnph.billspayment.test.generic.ResponseObject;

import java.util.List;

public class TestResponseString extends ResponseObject<List<Test>> {
    @Override
    public List<Test> getData() {
        return (List<Test>) this.data;
    }

    @Override
    public void setData(List<Test> data) {
        this.data = data;
    }
}
