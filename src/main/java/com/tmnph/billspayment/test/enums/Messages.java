package com.tmnph.billspayment.test.enums;

import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
public enum Messages {
    API_DOES_NOT_EXIST("0", "API does not exist"),
    SCOPE_ALREADY_EXIST("1", "Scope is already existed"),
    FAIL("2", "fail");

    private final String code;
    private final String description;

    private static final Map<String, String> MAP = new HashMap<String, String>();
    static {
        for (Messages s : Messages.values()) {
            MAP.put(s.code, s.description);
        }
    }

    Messages(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static String getDescriptionByCode(String code) {
        return MAP.get(code);
    }

//    public static Map<Integer, Messages> constants(Messages message) {
//        Map<Integer, Messages> map = new HashMap<Integer, Messages>();
//        for (Messages value: Messages.values()) {
//            if(value.key == message.key) map.put(Integer.valueOf(value.name()), value);
//        }
//        return map;
//    }


}
