package com.tmnph.billspayment.test.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tmnph.billspayment.test.entities.Test;
import com.tmnph.billspayment.test.repositories.TestRepository;
import com.tmnph.billspayment.test.services.TestService;
import com.tmnph.billspayment.webservices.ECPayWebService;
import com.truemoneyph.common.util.JsonUtil;

import ECPay.wsdl.ArrayOfBStruct;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class TestServiceImpl implements TestService {
	@Autowired
	private TestRepository tr;
	@Autowired
	private ECPayWebService ecpay;
	@Value("${spring.datasource.url}")
	private String message;

	@Override
	public Test get(long id) {
		log.info("TestServiceImpl get~~");
		log.info("long id: " + id);

		log.info("message: " + message);
		// user here is a prepopulated User instance
		Test t = tr.findFirstById(id);// modelMapper.map(tr.findById(id), Test.class);

		log.info("Test: " + JsonUtil.objectToJson(t));
		return t;
	}

	@Override
	public ArrayOfBStruct getBillerList() {
		ArrayOfBStruct ec = ecpay.getBillerList();
		return ec;
	}

	@Override
	public String test() {
		log.info("test");
		Map<String,Object>map =new HashMap<>();
		map.put("test","test");

		log.info(map);
//		log.log(log.getClass().toString(), Level.INFO, map, null);
		return null;
	}

}
