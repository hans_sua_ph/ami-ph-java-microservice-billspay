package com.tmnph.billspayment.test.services;

import com.tmnph.billspayment.test.entities.Test;

import ECPay.wsdl.ArrayOfBStruct;

public interface TestService {
	public Test get(long id);
	public ArrayOfBStruct getBillerList();
	public String test();
}
