package com.tmnph.billspayment.test.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ECPay.wsdl.BStruct;
import com.tmnph.billspayment.test.enums.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tmnph.billspayment.configs.PortalErrorCodeConfig;
import com.tmnph.billspayment.test.dto.EndpointResult;
import com.tmnph.billspayment.test.dto.TestResponse;
import com.tmnph.billspayment.test.dto.TestResponseMap;
import com.tmnph.billspayment.test.dto.TestResponseString;
import com.tmnph.billspayment.test.entities.Test;
import com.tmnph.billspayment.test.services.TestService;

import ECPay.wsdl.ArrayOfBStruct;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RefreshScope
@RestController
@RequestMapping(value = "/test", produces = "application/json")

// ben: removes the consumes since the route is still on the parent side and will cause an error of no path founnd.
// so the mapping localhost:9090/test is already expecting a parameter of json if hindi niya nakita magthrothrow na
// siya ng media type response mostly common.
// @RequestMapping(value = "/test", consumes = "application/json", produces = "application/json")
@Api(value = "testController", description = "test controller for microservice POC")
public class TestController {
	@Value("${message}")
	private String message;
	@Value("${spring.application.name}")
	private String name;

	@Autowired
	private TestService ts;
	@Autowired
	private PortalErrorCodeConfig ec;


	@ApiOperation(value = "get ecpay biller list", response = TestResponse.class)
	@GetMapping("/testECPay")
	public TestResponse testECPay() {
        TestResponse testResponse = new TestResponse();

//      ben: service is having ClassNotFoundException on the ecpay
//      Since the object is an xml and JsonUtil.objectToJson is having a hard time converting the object
//      it's only an assumption and still not sure on the design hehe
//      as workaround pa comment nalang ang JsonUtil.objectToJson sa ECPayWebService.java
//      method na getBillerList()

		log.info("message: " + message);
        log.info("testECPay");
//        ArrayOfBStruct result = ts.getBillerList();
//		log.info("result: " + JsonUtil.objectToJson(result));

        testResponse.setData(ts.getBillerList().getBStruct());
        return testResponse;
	}

	@JsonProperty("trm")
    @ApiOperation(value = "test message in config")
    @GetMapping("/testMsg")
    public TestResponseMap testMsg(@RequestParam("test") String test) {
        TestResponseMap trm = new TestResponseMap();
//        log.info("message: " + message);
//        log.info("name: " + name);
//        ts.test();
//        String error = ec.getErrorMessage(test);
//        Map<String, Object> result = new HashMap<>();
//        result.put("error", error);
//        log.error("error: "+ error);

        Convert convert = data -> {
            Map<String, String> result = new HashMap<String, String>();
            result.put("HI", (String) data);

            return result;
        };

        trm.setData((Map<String, String>) convert.convertObjectToList((String) test));

        return trm;
    }

    @JsonProperty("trs")
    @ApiModelProperty(value = "A list of [Test]referenced by the objects in the `objects` field.")
    @ApiOperation(value = "View all the Biller Categories available for selection from EC Pay.")
    @GetMapping("/getBillerList")
    public TestResponseString testGetBills(@RequestParam("nums") Integer indexs) {
        TestResponseString tr = new TestResponseString();
	    log.info("indexs: " + indexs);
        log.info(Messages.API_DOES_NOT_EXIST.getCode());
        log.info(Messages.API_DOES_NOT_EXIST.getDescription());
        Convert convert = data -> {
           List<Test> listResults = new ArrayList();
           for(int i = 0; i < (Integer) data; i++) {
               listResults.add(new Test());
           }
           return listResults;
       } ;


        tr.setData((List) convert.convertObjectToList(indexs));

        return tr;
    }

    public interface Convert{
	    Object convertObjectToList(Object obj);
    }
    
    @ApiOperation(value = "get ecpay biller list")
	@GetMapping("/ECPayGetBillerList")
	public EndpointResult<ArrayOfBStruct> ECPayGetBillerList() {
		EndpointResult<ArrayOfBStruct> result = new EndpointResult<ArrayOfBStruct>();
//      ben: service is having ClassNotFoundException on the ecpay
//      Since the object is an xml and JsonUtil.objectToJson is having a hard time converting the object
//      it's only an assumption and still not sure on the design hehe
//      as workaround pa comment nalang ang JsonUtil.objectToJson sa ECPayWebService.java
//      method na getBillerList()

		log.info("message: " + message);
		log.info("testECPay");

		result.addData(ts.getBillerList());

		return result;
	}

	@ApiOperation(value = "test message in config")
	@GetMapping("/testEndpointResult")
	public EndpointResult<Integer> testEndpointResult(@RequestParam("test") int test) {
		EndpointResult<Integer> result = new EndpointResult<Integer>();
		log.error("test: " + test);
		result.addData(test);
		log.error("test: " + result.getData());
		log.error("test: " + result.getData().get(0));
		return result;
	}

    @ApiOperation(value = "test message in config", response = Map.class)
    @GetMapping("/testErrorMsg")
    public EndpointResult<String> testErrorMsg(@RequestParam("test") String test) {
        EndpointResult<String> result = new EndpointResult<String>();
        log.info("message: " + message);
        log.info("name: " + name);
        ts.test();
        String error = ec.getErrorMessage(test);
        result.addError(error);
        log.error("error: " + error);
        return result;
    }


    @ApiOperation(value = "test message in config", response = EndpointResult.class)
    @GetMapping("/searchBillerList/")
    public EndpointResult<Object> searchBillerList(@RequestParam(name = "billerTag", required = false) String test) {
        EndpointResult<Object> result = new EndpointResult<Object>();
        ArrayOfBStruct bStructsLists = ts.getBillerList();

        for (BStruct bStruct: bStructsLists.getBStruct()) {
            if(bStruct.getBillerTag().equals(test)) result.addData(bStruct);
        }

        if(test == null || test.equals("")) result.addData(bStructsLists.getBStruct());
        else if(result.getData().isEmpty()) {
            result.setError();
            result.addError(Messages.getDescriptionByCode(Messages.FAIL.getCode()));
        }

        return result;
    }


}
