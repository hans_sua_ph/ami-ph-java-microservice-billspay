package com.tmnph.billspayment;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//@Log4j2
public class BillspaymentSpiApplicationTests {
	@Autowired
	private MockMvc mvc;

	@Test
	public void testECPay() throws Exception {
		mvc.perform(get("/test/testECPay")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.header("test", "0"))
				.andExpect(status().isOk());
//				.andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Matchers.is("ok")))
//				.andExpect(MockMvcResultMatchers.jsonPath("$.data.token").isNotEmpty());
	}

	@Test
	public void testMsgConfig() throws Exception {
		mvc.perform(get("/test/testMsg")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.header("test", "0").param("test", "-800"))
				.andExpect(status().isOk());
//				.andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Matchers.is("ok")))
//				.andExpect(MockMvcResultMatchers.jsonPath("$.data.token").isNotEmpty());
	}
}
